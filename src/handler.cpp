#ifdef SWLOC_HAVE_TBB

#include <swloc.h>
#include <execinfo.h>

extern "C"
{
#include "context.h"
#include "core.h"
}

#define SIZE 100
char tbb_default_num_threads_name[SIZE];
unsigned default_num_threads_name_init = 0;

typedef int (*real_tbb_default_num_threads)();
#define SWLOC_REAL_TBB_DEFAULT_NUM_THREADS() \
({                                                    \
        real_tbb_default_num_threads r_default_num_threads;    \
        r_default_num_threads = (real_tbb_default_num_threads) dlsym(RTLD_NEXT, tbb_default_num_threads_name);        \
        SWLOC_ASSERT_MSG(r_default_num_threads != NULL, "tbb::task_scheduler_init::default_num_threads is not found");               \
        /* return value */                              \
        r_default_num_threads();                   \
})

namespace tbb
{

	class task_scheduler_init
	{
		public :
		int default_num_threads();


	};

	int task_scheduler_init::default_num_threads()
	{
		if (swloc_core_is_initialized())
		{
			swloc_context_t ctx = swloc_core_get_context();

			if (ctx != NULL)
			{
				return ctx->nb_cores_avail + ctx->nb_cores_given;
			}
		}

		/*
		 * If the function was called outside SwLoc, call the original function.
		 * Du to mangled name, use a litle hack to get the correct name by finding it in the backtrace.
		 */
		if (!default_num_threads_name_init)
		{
			int nptrs;
			void *buffer[SIZE];
			char **strings;

			nptrs = backtrace(buffer, SIZE);

			strings = backtrace_symbols(buffer, nptrs);
			SWLOC_ASSERT_MSG(strings != NULL, "Cannot get mangled name of tbb::task_scheduler_init::default_num_threads !");

			//We are in the function, therefore, we use the first entry
			char* name = strings[0];
			/* 
			 * Format is "[path of file](name+things)[other things]
			 * We remove characters up to the first parenthesis included and after the '+'.
			 */
			while (*name != '(')
				name++;
			name++;

			char* tmp_name = name;
			while (*tmp_name != '+')
				tmp_name++;
			*tmp_name = '\0';

			strcpy(tbb_default_num_threads_name, name);
			default_num_threads_name_init = 1;

			free(strings);
		}

		return SWLOC_REAL_TBB_DEFAULT_NUM_THREADS();			
	}
}

#endif
