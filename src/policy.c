#include <swloc.h>

#include "context.h"
#include "core.h"
#include "policy.h"

static void swloc_policy_create(swloc_context_t ctx, enum swloc_policy policy, va_list params)
{
	ctx->policy = malloc(sizeof(*ctx->policy));
	ctx->policy->current_policy = policy;

        switch (policy)
        {
                case swloc_policy_explicit :
                {
			/* cores are specified in the next array */
                        ctx->policy->policy_explicit_cores = va_arg(params, unsigned *);
                        SWLOC_ASSERT_MSG(ctx->policy->policy_explicit_cores != NULL, "Policy swloc_policy_explicit needs to provide an array with cores to bind");
                        break;
                }

                case swloc_policy_next :
		case swloc_policy_socket :
		case swloc_policy_scatter :
			/* Nothing to do */
                        break;

                default :
                        SWLOC_ASSERT_MSG(0, "Policy not recognized (%u)", policy);
        }

}

int swloc_policy_set(enum swloc_policy policy, ...)
{
	swloc_context_t ctx = swloc_core_get_context();

        va_list params;
        va_start(params, policy);

        if ((ctx->father_ctx != NULL && ctx->policy != ctx->father_ctx->policy) || ctx->father_ctx == NULL)
                swloc_policy_internal_remove(ctx);

	swloc_policy_create(ctx, policy, params);

        va_end(params);

	return 0;
}

int swloc_policy_internal_set(swloc_context_t ctx, enum swloc_policy policy, ...)
{
        va_list params;
        va_start(params, policy);

        if ((ctx->father_ctx != NULL && ctx->policy != ctx->father_ctx->policy) || ctx->father_ctx == NULL)
                swloc_policy_internal_remove(ctx);

	swloc_policy_create(ctx, policy, params);

        va_end(params);

	return 0;
}

void swloc_policy_internal_remove(swloc_context_t ctx)
{
        free(ctx->policy);
}

enum swloc_policy swloc_policy_get()
{
	swloc_context_t local_context = swloc_core_get_context();
        return local_context->policy->current_policy;
}

