#include <stdlib.h>

#include <swloc.h>

#include "context.h"
#include "core.h"
#include "hypervisor.h"
#include "kernel.h"
#include "policy.h"
#include "utils.h"

static int swloc_context_cpu_request(swloc_context_t ctx, unsigned nb_cores, enum swloc_context_cores_status * cpu_cores_requested);
static int swloc_context_cpu_release(swloc_context_t ctx, unsigned nb_cores, enum swloc_context_cores_status * cpu_cores_released);
static int swloc_context_cpu_select_avail_and_given(unsigned nb_cores, enum swloc_context_cores_status * source, enum swloc_context_cores_status * selected);

swloc_context_t swloc_context_create(enum swloc_context_type type,...)
{
        swloc_context_t ctx;
        ctx = calloc(1, sizeof(*ctx));
        SWLOC_ASSERT_MSG(ctx != NULL, "swloc_context_create can not alloc memory resources (%zu bytes requested)", sizeof(*ctx));

        const unsigned nb_cores = swloc_core_cpu_global();

        ctx->nb_cores_avail = 0;
        ctx->nb_cores_given = 0;
        ctx->nb_cores_min = 1;
        ctx->nb_cores_max = nb_cores;
        ctx->nb_cores_begin = 0;

        ctx->nb_cores_to_modify = 0;

        ctx->omp_support = 0;
        ctx->next_core_pthread = 0;

	ctx->hp = NULL;

        swloc_context_t local_context = swloc_core_get_context();
        if (local_context != NULL)
        {
                pthread_mutex_lock(&local_context->mutex);
                ctx->nb_cores_max = local_context->nb_cores_max;
        }

        pthread_mutex_init(&ctx->mutex, NULL);
        ctx->nb_ctx_sons = 0;
        
        ctx->cores_avail = calloc(nb_cores, sizeof(*ctx->cores_avail));
        SWLOC_ASSERT_MSG(ctx->cores_avail != NULL, "swloc_kernel_start can not alloc memory resources (%zu bytes requested)", nb_cores*sizeof(*ctx->cores_avail));

        ctx->cores_to_modify = calloc(nb_cores, sizeof(*ctx->cores_to_modify));
        SWLOC_ASSERT_MSG(ctx->cores_to_modify != NULL, "swloc_kernel_start can not alloc memory resources (%zu bytes requested)", nb_cores*sizeof(*ctx->cores_to_modify));

        unsigned i;
        for (i = 0; i < nb_cores; i++)
                ctx->cores_to_modify[i] = SWLOC_CONTEXT_CORES_FREE;

        pthread_cond_init(&ctx->cond, NULL);

        va_list params;
        va_start(params, type);

        while (type != SWLOC_CONTEXT_END)
        {
                switch (type)
                {
                        case SWLOC_CONTEXT_NB_CPUS :
                        {
                                unsigned nb = va_arg(params, unsigned);
                                SWLOC_ASSERT_MSG(nb > 0, "Each context needs at least 1 CPU core to run !");
                                SWLOC_ASSERT_MSG(nb <= ctx->nb_cores_max , "Each context needs at maximum %d CPU core to run !", ctx->nb_cores_max);
                                ctx->nb_cores_min = nb;
                                ctx->nb_cores_max = nb;
                                ctx->nb_cores_begin = nb;
                                break;
                        }                

                        case SWLOC_CONTEXT_NB_CPUS_MIN :
                        {
                                unsigned nb = va_arg(params, unsigned);
                                SWLOC_ASSERT_MSG(nb > 0, "Each context needs at least 1 CPU core to run !");
                                SWLOC_ASSERT_MSG(nb <= ctx->nb_cores_max , "Each context needs at maximum %d CPU core to run !", ctx->nb_cores_max);
                                ctx->nb_cores_min = nb;
                                break;
                        }

                        case SWLOC_CONTEXT_NB_CPUS_MAX :
                        {
                                unsigned nb = va_arg(params, unsigned);
                                SWLOC_ASSERT_MSG(nb > 0, "Each context needs at least 1 CPU core to run !");
                                SWLOC_ASSERT_MSG(nb <= ctx->nb_cores_max , "Each context needs at maximum %d CPU core to run !", ctx->nb_cores_max);
                                ctx->nb_cores_max = nb;
                                break;
                        }

                        case SWLOC_CONTEXT_NB_CPUS_START :
                        {
                                unsigned nb = va_arg(params, unsigned);
                                SWLOC_ASSERT_MSG(nb > 0, "Each context needs at least 1 CPU core to run !");
                                SWLOC_ASSERT_MSG(nb <= ctx->nb_cores_max , "Each context needs at maximum %d CPU core to run !", ctx->nb_cores_max);
                                ctx->nb_cores_begin = nb;
                                break;
                        }

                        case SWLOC_CONTEXT_NB_CPUS_HALF :
                        {
                                unsigned nb = ctx->nb_cores_max/2;
                                SWLOC_ASSERT_MSG(nb > 0, "Each context needs at least 1 CPU core to run !");
                                ctx->nb_cores_min = nb;
                                ctx->nb_cores_max = nb;
                                ctx->nb_cores_begin = nb;
                                break;
                        }

                        case SWLOC_CONTEXT_NB_CPUS_QUARTER :
                        {
                                unsigned nb = ctx->nb_cores_max/4;
                                SWLOC_ASSERT_MSG(nb > 0, "Each context needs at least 1 CPU core to run !");
                                ctx->nb_cores_min = nb;
                                ctx->nb_cores_max = nb;
                                ctx->nb_cores_begin = nb;
                                break;
                        }

                        case SWLOC_CONTEXT_NB_CPUS_THREE_QUARTERS :
                        {
                                unsigned nb = (3*ctx->nb_cores_max)/4;
                                SWLOC_ASSERT_MSG(nb > 0, "Each context needs at least 1 CPU core to run !");
                                ctx->nb_cores_min = nb;
                                ctx->nb_cores_max = nb;
                                ctx->nb_cores_begin = nb;
                                break;
                        }

                        case SWLOC_CONTEXT_OMP_SUPPORT :
                        {
#ifndef SWLOC_HAVE_OPENMP
                                SWLOC_PRINT_MSG("OMP support is not enabled. SWLOC_CONTEXT_OMP_SUPPORT is skipped");
#else
                                ctx->omp_support = 1;
#endif
                                break;
                        }

                        case SWLOC_CONTEXT_TBB_SUPPORT:
                        {
#ifndef SWLOC_HAVE_TBB
                                SWLOC_PRINT_MSG("TBB support is not enabled. SWLOC_CONTEXT_TBB_SUPPORT is skipped");
#else
                                ctx->tbb_support = 1;
#endif
                                break;
                        }

                        default :
                                SWLOC_ASSERT_MSG(0, "Type not recognized (%u)", type);
                }

		type = va_arg(params, enum swloc_context_type);
        }

        va_end(params);

        //TODO : check options

        /* give min core to begin */
        if (ctx->nb_cores_begin == 0)
                ctx->nb_cores_begin = ctx->nb_cores_min;

        if (local_context != NULL)
        {
		ctx->policy = local_context->policy;

                int res = swloc_context_cpu_request(local_context, ctx->nb_cores_begin, ctx->cores_avail);
                SWLOC_ASSERT_MSG(res == 0 || local_context->hp != NULL, "Context can not request %u cores (%u available) !", ctx->nb_cores_begin, local_context->nb_cores_avail);
                if (res == 0)
                        ctx->nb_cores_avail = ctx->nb_cores_begin;
                local_context->nb_ctx_sons++;
        }
        else
        {
		swloc_policy_internal_set(ctx, swloc_policy_next);

                unsigned i;
                for (i = 0; i < nb_cores; i++)
                        ctx->cores_avail[i] = SWLOC_CONTEXT_CORES_AVAIL;

                ctx->nb_cores_avail = nb_cores;
                /* Global context does not need core to run */
                ctx->nb_cores_min = 0;
        }

        if (local_context != NULL)
                pthread_mutex_unlock(&local_context->mutex);

        ctx->father_ctx = local_context;

        ctx->nb_ctx_sons_reclaim = 0;
        ctx->status = SWLOC_CONTEXT_STATUS_WAITING;
        if (local_context != NULL && local_context->hp)
                swloc_hypervisor_track_context(local_context->hp, ctx);
        
        return ctx;
}

int swloc_context_destroy(swloc_context_t ctx)
{
        swloc_context_t local_context = swloc_core_get_context();
        if (local_context != NULL && local_context->hp)
                swloc_hypervisor_untrack_context(local_context->hp, ctx);

        if (ctx->father_ctx != NULL)
        {
                pthread_mutex_lock(&ctx->father_ctx->mutex);
                swloc_context_cpu_release(ctx->father_ctx, ctx->nb_cores_avail, ctx->cores_avail);
                pthread_mutex_unlock(&ctx->father_ctx->mutex);

                if (ctx->policy != ctx->father_ctx->policy)
                        swloc_policy_internal_remove(ctx);
        }
        else
        {
                /* Root context has the last policy */
                swloc_policy_internal_remove(ctx);
        }

        pthread_mutex_destroy(&ctx->mutex);

        free(ctx->cores_avail);
        free(ctx->cores_to_modify);
        free(ctx);

        return 0;
}

/* Check core which is AVAIL : for thread created by swloc */
unsigned swloc_context_get_n_core_avail(swloc_context_t ctx, unsigned n)
{
        unsigned i;
        unsigned nb = 0;
        unsigned nb_cores = swloc_core_cpu_global();
        for(i = 0; i < nb_cores; i++)
        {
                if (ctx->cores_avail[i] == SWLOC_CONTEXT_CORES_AVAIL && nb == n)
                        return i;
                if (ctx->cores_avail[i] == SWLOC_CONTEXT_CORES_AVAIL)
                        nb++;
        }

        SWLOC_ASSERT_MSG(0, "Don't find enough cores (currently %u | expected %u)", nb, n);
        return 0;
}

/* Check core which is GIVEN and AVAIL : for thread created by other libraries or by user itself */
unsigned swloc_context_get_n_core_avail_and_given(swloc_context_t ctx, unsigned n)
{
        unsigned i;
        unsigned nb = 0;
        unsigned nb_cores = swloc_core_cpu_global();
        for(i = 0; i < nb_cores; i++)
        {
                if ((ctx->cores_avail[i] == SWLOC_CONTEXT_CORES_AVAIL || ctx->cores_avail[i] == SWLOC_CONTEXT_CORES_GIVEN) && nb == n)
                        return i;
                if (ctx->cores_avail[i] == SWLOC_CONTEXT_CORES_AVAIL || ctx->cores_avail[i] == SWLOC_CONTEXT_CORES_GIVEN)
                        nb++;
        }

        SWLOC_ASSERT_MSG(0, "Don't find enough cores (currently %u | expected %u)", nb, n);
        return 0;
}

/* mutex in father_ctx of ctx must be already taken */
unsigned swloc_context_modify_cores(swloc_context_t ctx, swloc_kernel_t knl, int cores)
{
        SWLOC_ASSERT_MSG(ctx->nb_cores_to_modify == 0, "A request to modify core is already pending");
        SWLOC_ASSERT(cores != 0);

        unsigned cores_abs = (cores > 0) ? cores : -cores;

        ctx->nb_cores_to_modify = cores;

        if (cores > 0)
        {
                swloc_context_cpu_request(ctx->father_ctx, cores_abs, ctx->cores_to_modify);
        }
        else
        {
                swloc_context_cpu_select_avail_and_given(cores_abs, ctx->cores_avail, ctx->cores_to_modify);
        }

        if (ctx->status == SWLOC_CONTEXT_STATUS_WAITING)
                swloc_context_ack_cores(ctx);
        else
                knl->options->handler_modify_cpu_cores(ctx, cores);
        
        return 0;
}

//mutex in context need to be already taken
void swloc_context_ack_cores(swloc_context_t ctx)
{
        SWLOC_ASSERT(ctx);
        SWLOC_ASSERT(ctx->nb_cores_to_modify != 0);

        if (ctx->nb_cores_to_modify > 0)
        {
                unsigned nb_cores = swloc_core_cpu_global();
                int cnt = 0;
                unsigned i;
                for(i = 0; i < nb_cores; i++)
                        if (ctx->cores_to_modify[i] == SWLOC_CONTEXT_CORES_AVAIL)
                        {
                                ctx->cores_to_modify[i] = SWLOC_CONTEXT_CORES_FREE;
                                ctx->cores_avail[i] = SWLOC_CONTEXT_CORES_AVAIL;
                                cnt++;
                        }

                SWLOC_ASSERT(cnt == ctx->nb_cores_to_modify);
        }
        else
        {
                unsigned nb_cores = swloc_core_cpu_global();
                unsigned cnt = 0;
                unsigned i;
                for(i = 0; i < nb_cores; i++)
                        if (ctx->cores_to_modify[i] == SWLOC_CONTEXT_CORES_AVAIL)
                        {
                                ctx->cores_avail[i] = SWLOC_CONTEXT_CORES_FREE;
                                ctx->father_ctx->cores_avail[i] = SWLOC_CONTEXT_CORES_AVAIL;
                                ctx->cores_to_modify[i] = SWLOC_CONTEXT_CORES_FREE;

                                cnt++;
                        }

                //nb_cores_to_modify is negative
                unsigned cores = -ctx->nb_cores_to_modify;
                SWLOC_ASSERT(cnt == cores);
                ctx->father_ctx->nb_cores_avail += cores;
        }


        ctx->nb_cores_avail += ctx->nb_cores_to_modify;
        ctx->nb_cores_to_modify = 0;

#ifdef SWLOC_HAVE_OPENMP
	if (ctx->omp_support)
		swloc_kernel_omp_set_thread(ctx);
#endif

}

hwloc_bitmap_t swloc_context_get_cores(swloc_context_t ctx)
{
	hwloc_topology_t topology = swloc_core_get_hwloc_topology();
        hwloc_bitmap_t set = hwloc_bitmap_alloc();
        hwloc_bitmap_zero(set);
        
        unsigned i;
        unsigned nb_cores = swloc_core_cpu_global();
        for(i = 0; i < nb_cores; i++)
        {
                if (ctx->cores_avail[i] == SWLOC_CONTEXT_CORES_AVAIL || ctx->cores_avail[i] == SWLOC_CONTEXT_CORES_GIVEN)
		{
                        hwloc_obj_t obj = hwloc_get_obj_by_type(topology, HWLOC_OBJ_CORE, i);
                        hwloc_bitmap_or(set, set, obj->cpuset);
		}
        }

        return set;
}

/* This function takes nb_cores cores in the context ctx. Its mutex need to be already taken */
static int swloc_context_cpu_request(swloc_context_t ctx, unsigned nb_cores, enum swloc_context_cores_status * cpu_cores_requested)
{
	unsigned nb_cores_given = 0;
        unsigned nb_cores_global = swloc_core_cpu_global();

        if (ctx->nb_cores_avail < nb_cores)
                return -1;

        switch (ctx->policy->current_policy)
        {
                case swloc_policy_next :
                {
                        unsigned i;
                        for (i = 0; i < nb_cores_global; i++)
                        {
                                if (nb_cores_given == nb_cores)
                                        break;

                                if (ctx->cores_avail[i] == SWLOC_CONTEXT_CORES_AVAIL)
                                {
                                        ctx->cores_avail[i] = SWLOC_CONTEXT_CORES_GIVEN;
                                        cpu_cores_requested[i] = SWLOC_CONTEXT_CORES_AVAIL;
					nb_cores_given++;
                                }
                        }
                        break;
                }

                case swloc_policy_explicit :
                {
			unsigned cores_used = nb_cores_global - ctx->nb_cores_avail;

			unsigned i;
			for (i = cores_used; i < cores_used + nb_cores; i++)
			{
				unsigned core = ctx->policy->policy_explicit_cores[i];

				SWLOC_ASSERT_MSG(ctx->cores_avail[core] == SWLOC_CONTEXT_CORES_AVAIL, "Core %u from policy explicit is already assigned", core);

				ctx->cores_avail[core] = SWLOC_CONTEXT_CORES_GIVEN;
				cpu_cores_requested[core] = SWLOC_CONTEXT_CORES_AVAIL;
				nb_cores_given++;
			}

			break; 
                }

		case swloc_policy_socket :
		{
			hwloc_topology_t topology = swloc_core_get_hwloc_topology();
			int nb_sockets = hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_PACKAGE);
			SWLOC_ASSERT_MSG(nb_sockets > 0, "Error when discovering socket or no socket found (%d) !", nb_sockets);

			unsigned * free_cores_per_sockets = calloc(nb_sockets, sizeof(*free_cores_per_sockets));

			unsigned i;
			/* Count number of available cores in each socket */
			for (i = 0; i < nb_cores_global; i++)
			{
				if (ctx->cores_avail[i] == SWLOC_CONTEXT_CORES_AVAIL)
				{
					hwloc_obj_t obj_core = hwloc_get_obj_by_type(topology, HWLOC_OBJ_CORE, i);
					/* Get the socket associated */
					hwloc_obj_t obj_socket = hwloc_get_ancestor_obj_by_type(topology, HWLOC_OBJ_PACKAGE, obj_core);
					/* count it */
					free_cores_per_sockets[obj_socket->logical_index]++;
				}
			}

			while (nb_cores_given != nb_cores)
			{
				/* find if a socket can give the entire set of cores */
				for (i = 0; i < (unsigned) nb_sockets; i++)
				{
					if (free_cores_per_sockets[i] >= nb_cores)
					{
						/* Find cores with this socket */
						unsigned c;
						for (c = 0; c < nb_cores_global && nb_cores_given != nb_cores; c++)
						{
							if (ctx->cores_avail[c] == SWLOC_CONTEXT_CORES_AVAIL)
							{
								hwloc_obj_t obj_core = hwloc_get_obj_by_type(topology, HWLOC_OBJ_CORE, c);
								hwloc_obj_t obj_socket = hwloc_get_ancestor_obj_by_type(topology, HWLOC_OBJ_PACKAGE, obj_core);

								if (obj_socket->logical_index == i)
								{
									ctx->cores_avail[c] = SWLOC_CONTEXT_CORES_GIVEN;
									cpu_cores_requested[c] = SWLOC_CONTEXT_CORES_AVAIL;
									nb_cores_given++;
									free_cores_per_sockets[obj_socket->logical_index]--;
								}
							}
						}
					}
				}

				if (nb_cores_given != nb_cores)
				{
					unsigned socket = 0;
					/* Select the socket with the highest number of available cores */
					for (i = 1; i < (unsigned) nb_sockets; i++)
					{
						if (free_cores_per_sockets[i] > free_cores_per_sockets[socket])
							socket = i;
					}

					for (i = 0; i < nb_cores_global && nb_cores_given != nb_cores; i++)
					{
						if (ctx->cores_avail[i] == SWLOC_CONTEXT_CORES_AVAIL)
						{
							hwloc_obj_t obj_core = hwloc_get_obj_by_type(topology, HWLOC_OBJ_CORE, i);
							hwloc_obj_t obj_socket = hwloc_get_ancestor_obj_by_type(topology, HWLOC_OBJ_PACKAGE, obj_core);

							if (obj_socket->logical_index == socket)
							{
								ctx->cores_avail[i] = SWLOC_CONTEXT_CORES_GIVEN;
								cpu_cores_requested[i] = SWLOC_CONTEXT_CORES_AVAIL;
								nb_cores_given++;
								free_cores_per_sockets[obj_socket->logical_index]--;
							}
						}
					}
				}
			}

			free(free_cores_per_sockets);
			break;
		}

		case swloc_policy_scatter :
		{
			hwloc_topology_t topology = swloc_core_get_hwloc_topology();
			int nb_sockets = hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_PACKAGE);
			SWLOC_ASSERT_MSG(nb_sockets > 0, "Error when discovering socket or no socket found (%d) !", nb_sockets);

			unsigned * used_cores_per_sockets = calloc(nb_sockets, sizeof(*used_cores_per_sockets));

			while (nb_cores_given != nb_cores)
			{
				unsigned min = nb_cores_global;
				/* compute the minimum of cores given to each socket */
				unsigned i;
				for (i = 0; i < (unsigned) nb_sockets; i++)
				{
					if (used_cores_per_sockets[i] < min)
						min = used_cores_per_sockets[i];
				}

				unsigned c;
				for (c = 0; c < nb_cores_global && nb_cores_given != nb_cores; c++)
				{
					if (ctx->cores_avail[c] == SWLOC_CONTEXT_CORES_AVAIL)
					{
						hwloc_obj_t obj_core = hwloc_get_obj_by_type(topology, HWLOC_OBJ_CORE, c);
						hwloc_obj_t obj_socket = hwloc_get_ancestor_obj_by_type(topology, HWLOC_OBJ_PACKAGE, obj_core);

						if (used_cores_per_sockets[obj_socket->logical_index] == min)
						{
							ctx->cores_avail[c] = SWLOC_CONTEXT_CORES_GIVEN;
							cpu_cores_requested[c] = SWLOC_CONTEXT_CORES_AVAIL;
							nb_cores_given++;
							used_cores_per_sockets[obj_socket->logical_index]++;
						}
					}
				}

				for (i = 0; i < (unsigned) nb_sockets; i++)
				{
					/* if value was not modified previously, it means the socket is full */
					if (used_cores_per_sockets[i] == min)
						used_cores_per_sockets[i] = nb_cores_global;
				}


			}

			free(used_cores_per_sockets);			
			break;
		}

                default :
                {
                        SWLOC_ASSERT_MSG(0, "Policy is not recognized !");
                        break;
                }

        }

        ctx->nb_cores_avail -= nb_cores;
	ctx->nb_cores_given += nb_cores_given;

        SWLOC_ASSERT_MSG(nb_cores_given == nb_cores, "%u CPU cores requested are not provided", nb_cores - nb_cores_given);

        return 0;
}

//static int swloc_context_cpu_select_given(unsigned nb_cores, enum swloc_context_cores_status * source, enum swloc_context_cores_status * selected)
//{
//        //TODO apply policy
//        unsigned nb_cores_global = swloc_core_cpu_global();
//        unsigned i;
//        for (i = 0; i < nb_cores_global; i++)
//        {
//                if (source[i] == SWLOC_CONTEXT_CORES_GIVEN && nb_cores > 0)
//                {
//                        selected[i] = SWLOC_CONTEXT_CORES_GIVEN;
//                        nb_cores--;
//                }
//                if (nb_cores == 0)
//                        break;
//        }
//
//        SWLOC_ASSERT_MSG(nb_cores == 0, "%u CPU cores requested are not selected", nb_cores);
//
//        return 0;
//}
//
//static int swloc_context_cpu_select_avail(unsigned nb_cores, enum swloc_context_cores_status * source, enum swloc_context_cores_status * selected)
//{
//        //TODO apply policy
//        unsigned nb_cores_global = swloc_core_cpu_global();
//        unsigned i;
//        for (i = 0; i < nb_cores_global; i++)
//        {
//                if (source[i] == SWLOC_CONTEXT_CORES_AVAIL && nb_cores > 0)
//                {
//                        selected[i] = SWLOC_CONTEXT_CORES_AVAIL;
//                        nb_cores--;
//                }
//                if (nb_cores == 0)
//                        break;
//        }
//
//        SWLOC_ASSERT_MSG(nb_cores == 0, "%u CPU cores requested are not selected", nb_cores);
//
//        return 0;
//}

static int swloc_context_cpu_select_avail_and_given(unsigned nb_cores, enum swloc_context_cores_status * source, enum swloc_context_cores_status * selected)
{
        //TODO apply policy
        unsigned nb_cores_global = swloc_core_cpu_global();
        unsigned i;
        for (i = 0; i < nb_cores_global; i++)
        {
                if (source[i] == SWLOC_CONTEXT_CORES_AVAIL && nb_cores > 0)
                {
                        selected[i] = SWLOC_CONTEXT_CORES_AVAIL;
                        nb_cores--;
                }
                if (nb_cores == 0)
                        break;
        }

        for (i = 0; i < nb_cores_global; i++)
        {
                if (source[i] == SWLOC_CONTEXT_CORES_GIVEN && nb_cores > 0)
                {
                        selected[i] = SWLOC_CONTEXT_CORES_AVAIL;
                        nb_cores--;
                }
                if (nb_cores == 0)
                        break;
        }

        SWLOC_ASSERT_MSG(nb_cores == 0, "%u CPU cores requested are not selected", nb_cores);

        return 0;
}

/* This function release nb_cores in context ctx. Its mutex need to be already taken */
static int swloc_context_cpu_release(swloc_context_t ctx, unsigned nb_cores, enum swloc_context_cores_status * cpu_cores_released)
{
        ctx->nb_cores_avail += nb_cores;

        unsigned nb_cores_global = swloc_core_cpu_global();
        unsigned i;
        for (i = 0; i < nb_cores_global; i++)
        {
                if ((cpu_cores_released[i] == SWLOC_CONTEXT_CORES_GIVEN || cpu_cores_released[i] == SWLOC_CONTEXT_CORES_AVAIL) && nb_cores > 0)
                {
                        cpu_cores_released[i] = SWLOC_CONTEXT_CORES_FREE;
                        ctx->cores_avail[i] = SWLOC_CONTEXT_CORES_AVAIL;
                        nb_cores--;
                }
                if (nb_cores == 0)
                        break;
        }

        SWLOC_ASSERT_MSG(nb_cores == 0, "%u CPU cores requested are not released", nb_cores);

        return 0;
}
