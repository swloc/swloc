#ifndef SWLOC_INTERNAL_HYPERVISOR_H
#define SWLOC_INTERNAL_HYPERVISOR_H

struct swloc_hypervisor
{
        struct swloc_hypervisor_context_track * contexts;
	void * (*func_run)(struct swloc_hypervisor *s);	

        pthread_t thread;
        pthread_mutex_t mutex;
        unsigned run;
};


struct swloc_hypervisor_context_track
{
        swloc_context_t context;
        struct swloc_hypervisor_context_track *prev, *next; /* Needed by utlist.h */
};


int swloc_hypervisor_destroy(struct swloc_hypervisor * hp);

int swloc_hypervisor_track_context(struct swloc_hypervisor * hp, swloc_context_t context);
int swloc_hypervisor_untrack_context(struct swloc_hypervisor * hp, swloc_context_t context);

enum swloc_hypervisor_type swloc_hypervisor_convert_type(char * str);

#endif /* SWLOC_INTERNAL_HYPERVISOR_H */

