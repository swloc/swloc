#ifndef SWLOC_INTERNAL_POLICY_H
#define SWLOC_INTERNAL_POLICY_H

struct swloc_policy_info
{
	/* policy currently used */
	enum swloc_policy current_policy;

	/* contains core indexes used by swloc_policy_explicit */
	unsigned * policy_explicit_cores;
};

int swloc_policy_internal_set(swloc_context_t ctx, enum swloc_policy policy, ...);
void swloc_policy_internal_remove(swloc_context_t ctx);

#endif /* SWLOC_INTERNAL_POLICY_H */
