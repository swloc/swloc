#include <stdio.h>

#include <hwloc.h>

#include <swloc_handler.h>
#include "context.h"
#include "core.h"
#include "kernel.h"

int hwloc_topology_load (hwloc_topology_t topology)
{

        int err = SWLOC_REAL_HWLOC_TOPOLOGY_LOAD(topology);

	if (swloc_core_is_initialized())
	{
		swloc_context_t ctx = swloc_core_get_context();

		/* if hwloc_topology_load is called outside a kernel, we return the complete view 
		 * but, inside we remove some parts */
		if (ctx != NULL)
		{
			hwloc_bitmap_t cpuset = hwloc_bitmap_alloc();
			hwloc_bitmap_zero(cpuset);

			unsigned nb_cores_global = swloc_core_cpu_global();
			unsigned i;
			for (i = 0; i < nb_cores_global; i++)
				if (ctx->cores_avail[i])
				{
					hwloc_obj_t obj = hwloc_get_obj_by_type(topology, HWLOC_OBJ_CORE, i);
					hwloc_bitmap_or(cpuset, cpuset, obj->cpuset);
				}

			err |= hwloc_topology_restrict(topology, cpuset, 0);

			hwloc_bitmap_free(cpuset);
		}
	}

        return err;
}

struct swloc_pthread_create_parameter
{
        void * arg;
        swloc_context_t ctx;
        void *(*start_routine) (void *);
};

static void * swloc_pthread_create_wrapper(void * arg)
{
        struct swloc_pthread_create_parameter * params = (struct swloc_pthread_create_parameter *) arg;

        swloc_core_set_context(params->ctx);

        pthread_mutex_lock(&params->ctx->mutex);

        /* Bind thread */
        unsigned next_core = params->ctx->next_core_pthread;
        params->ctx->next_core_pthread = (params->ctx->next_core_pthread+1)%(params->ctx->nb_cores_avail+params->ctx->nb_cores_given);
        swloc_kernel_internal_bind_threads(params->ctx, next_core, 0);

        pthread_mutex_unlock(&params->ctx->mutex);

        void * res = params->start_routine(params->arg);

        free(arg);

        return res;
}

int pthread_create(pthread_t *thread, const pthread_attr_t *attr, void *(*start_routine) (void *), void *arg)
{
	if (swloc_core_is_initialized())
	{
		swloc_context_t ctx = swloc_core_get_context();

		if (ctx != NULL)
		{
			struct swloc_pthread_create_parameter * params = malloc(sizeof(*params));
			SWLOC_ASSERT(params != NULL);

			params->ctx = swloc_core_get_context();
			params->arg = arg;
			params->start_routine = start_routine;

			return SWLOC_REAL_PTHREAD_CREATE(thread, attr, swloc_pthread_create_wrapper, (void *)params);
		}
	}

	/* in case of swloc was not initialized or call is outside a context, do as usual */
	return SWLOC_REAL_PTHREAD_CREATE(thread, attr, start_routine, (void *)arg);
}
