#ifndef SWLOC_INTERNAL_CONTEXT_H
#define SWLOC_INTERNAL_CONTEXT_H

#include <swloc.h>
#include <pthread.h>

enum swloc_context_internal_status 
{
        /* Context is waiting a function to become a kernel
         * We can modify resources as we want between context's limits */
        SWLOC_CONTEXT_STATUS_WAITING,
        /* Context is running a function (ie kernel)
         * To modify resources, we need to call the callback provided
         * We can add cores and ask to reclaim cores */
        SWLOC_CONTEXT_STATUS_RUNNING,
        /* Context is running a function (ie kernel)
         * To modify resources, we need to call the callback provided
         * We can NOT add cores because we are reclaiming them */
        SWLOC_CONTEXT_STATUS_RECLAIM
};

enum swloc_context_cores_status
{
        SWLOC_CONTEXT_CORES_FREE,
        SWLOC_CONTEXT_CORES_AVAIL,
        SWLOC_CONTEXT_CORES_GIVEN
};

struct swloc_context
{
        enum swloc_context_cores_status * cores_avail;
        unsigned nb_cores_avail;
        unsigned nb_cores_given;
        unsigned nb_cores_min;
        unsigned nb_cores_max;
        unsigned nb_cores_begin;

        int nb_cores_to_modify;
        enum swloc_context_cores_status * cores_to_modify;

        unsigned omp_support;
        unsigned tbb_support;
        unsigned next_core_pthread;
        pthread_mutex_t mutex;
        pthread_cond_t cond;

        swloc_context_t father_ctx;
        unsigned nb_ctx_sons;

        swloc_kernel_t knl;
	struct swloc_hypervisor * hp;
        enum swloc_context_internal_status status;              /* used to apply strategy with hypervisors */
        unsigned nb_ctx_sons_reclaim;
	struct swloc_policy_info * policy;
};

unsigned swloc_context_get_n_core_avail(swloc_context_t ctx, unsigned n);
unsigned swloc_context_get_n_core_avail_and_given(swloc_context_t ctx, unsigned n);
unsigned swloc_context_modify_cores(swloc_context_t ctx, swloc_kernel_t knl, int cores);

#endif /* SWLOC_INTERNAL_CONTEXT_H */

