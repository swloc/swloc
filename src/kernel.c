#include <hwloc.h>
#ifdef SWLOC_HAVE_OPENMP 
#include <omp.h>
#endif
#include <stdlib.h>

#include "context.h"
#include "core.h"
#include "hypervisor.h"
#include "kernel.h"

#ifdef SWLOC_HAVE_TBB
#include "tbb.h"
#endif

void swloc_kernel_internal_bind_threads(swloc_context_t ctx, int rank, unsigned internal)
{
        hwloc_obj_t obj;
        hwloc_bitmap_t set;

	unsigned core;
	/* currently internal is used to define if thread is created by swloc or not */
	if (internal)
		core = swloc_context_get_n_core_avail(ctx, (unsigned) rank);
	else
		core = swloc_context_get_n_core_avail_and_given(ctx, (unsigned) rank);

        hwloc_topology_t topology = swloc_core_get_hwloc_topology();
        obj = hwloc_get_obj_by_type(topology, HWLOC_OBJ_CORE, core);

        set = obj->cpuset;

        hwloc_set_cpubind(topology, set, HWLOC_CPUBIND_THREAD);
}

#ifdef SWLOC_HAVE_OPENMP
void swloc_kernel_omp_set_thread(swloc_context_t ctx)
{
	omp_set_num_threads(ctx->nb_cores_avail);

	#pragma omp parallel default(shared)
	{
		int tid = omp_get_thread_num();
		swloc_kernel_internal_bind_threads(ctx, tid, 1);
	}
}
#endif

static void swloc_kernel_set_cores(swloc_context_t ctx)
{
#ifdef SWLOC_HAVE_OPENMP
        if (ctx->omp_support)
        {
                omp_set_num_threads(ctx->nb_cores_avail);

#pragma omp parallel
                {
                        /* Little hack to force OpenMP to create threads before entering
                         * in the mutex area below and having a deadlock */
                        ;
                }
        }
#endif

        pthread_mutex_lock(&ctx->mutex);

        /* Bind main thread */
        unsigned next_core = ctx->next_core_pthread;
        ctx->next_core_pthread = (ctx->next_core_pthread+1)%ctx->nb_cores_avail;
        swloc_kernel_internal_bind_threads(ctx, next_core, 1);

#ifdef SWLOC_HAVE_OPENMP
        if (ctx->omp_support)
        {
		swloc_kernel_omp_set_thread(ctx);
        }
#endif

        pthread_mutex_unlock(&ctx->mutex);

}

static void * swloc_kernel_launcher(void * arg)
{
        struct swloc_kernel_arg * kernel_arg = (struct swloc_kernel_arg *) arg;

        pthread_mutex_lock(&kernel_arg->ctx->mutex);

        while (kernel_arg->ctx->nb_cores_avail < kernel_arg->ctx->nb_cores_min)
        {
                pthread_cond_wait(&kernel_arg->ctx->cond, &kernel_arg->ctx->mutex);
        }

        pthread_mutex_unlock(&kernel_arg->ctx->mutex);

        kernel_arg->ctx->status = SWLOC_CONTEXT_STATUS_RUNNING;

        swloc_core_set_context(kernel_arg->ctx);

        swloc_kernel_set_cores(kernel_arg->ctx);

#ifdef SWLOC_HAVE_TBB
	if (kernel_arg->ctx->tbb_support)
		swloc_tbb_launch_kernel(kernel_arg);
	else
#endif
        kernel_arg->options->func_main(kernel_arg->options->argc, kernel_arg->options->argv);

        free(kernel_arg->options);
        free(kernel_arg);

        return NULL;
}

int swloc_kernel_options_init(struct swloc_kernel_options * options)
{
        options->func_main = NULL;
        options->argc = 0;
        options->argv = NULL;
        options->handler_modify_cpu_cores = NULL;

        return 0;
}

static void swloc_kernel_check_options(struct swloc_kernel_options * options)
{
        SWLOC_ASSERT_MSG(options != NULL, "Options given can't be NULL");
        SWLOC_ASSERT_MSG(options->func_main != NULL, "Main function given can't be NULL");

        if (options->argc > 0)
                SWLOC_ASSERT_MSG(options->argv != NULL, "Argument argv given can't be NULL if argc is different than 0");
}

static void swloc_kernel_duplicate_options(struct swloc_kernel_options * from, struct swloc_kernel_options * to)
{
        memcpy(to, from, sizeof(struct swloc_kernel_options));
}

swloc_kernel_t swloc_kernel_start(swloc_context_t ctx, struct swloc_kernel_options * options)
{
        swloc_kernel_check_options(options);

        struct swloc_kernel_options * save_opt = malloc(sizeof(*save_opt));
        swloc_kernel_duplicate_options(options, save_opt);

        swloc_kernel_t kernel;
        kernel = calloc(1, sizeof(*kernel));
        SWLOC_ASSERT_MSG(kernel != NULL, "swloc_kernel_start can not alloc memory resources (%zu bytes requested)", sizeof(*kernel));

        struct swloc_kernel_arg * arg;
        arg = calloc(1, sizeof(*arg));
        SWLOC_ASSERT_MSG(arg != NULL, "swloc_kernel_start can not alloc memory resources (%zu bytes requested)", sizeof(*arg));

        /* Restart index of core where the next pthread created will be binded */
        ctx->next_core_pthread = 0;

        kernel->ctx = ctx;
        kernel->options = save_opt;

        arg->options = save_opt;
        arg->ctx = ctx;
        
	swloc_context_t global_context = swloc_core_get_context();
        if (global_context->hp)
                SWLOC_ASSERT_MSG(options->handler_modify_cpu_cores, "Need to provide a handler if you activate the hypervisor !");
 
        /* Update context */
        ctx->knl = kernel;

        int res = SWLOC_REAL_PTHREAD_CREATE(&kernel->thread, NULL, swloc_kernel_launcher, (void *) arg);
        SWLOC_ASSERT_MSG(res == 0, "swloc_kernel_start can not launch the kernel (res = %d)", res);

        return kernel;
}

int swloc_kernel_wait(swloc_kernel_t knl)
{
        int res = pthread_join(knl->thread, NULL);
        SWLOC_ASSERT_MSG(res == 0, "swloc_kernel_wait can not wait the kernel");

        if (knl->ctx->nb_cores_to_modify != 0)
        {
                //TODO: make the request in manual
                SWLOC_ASSERT_MSG(0, "TODO not available");
        }

        /* Update context */
        knl->ctx->status = SWLOC_CONTEXT_STATUS_WAITING;
        knl->ctx->knl = NULL;

        free(knl);

        return 0;
}
