#ifndef SWLOC_INTERNAL_HYPERVISOR_EAGER_H
#define SWLOC_INTERNAL_HYPERVISOR_EAGER_H

void * swloc_hypervisor_eager(struct swloc_hypervisor * hp);

#endif /* SWLOC_INTERNAL_HYPERVISOR_EAGER_H */ 
