#ifndef SWLOC_INTERNAL_UTILS_H
#define SWLOC_INTERNAL_UTILS_H

#include <assert.h>
#include <stdio.h>

#define SWLOC_ASSERT_MSG(x, msg, ...)           \
do {                                            \
        if (!(x))                               \
        {                                       \
                fprintf(stderr, "\n[swloc][%s] " msg "\n\n", __func__, ## __VA_ARGS__); \
                assert(x);                      \
        }                                       \
} while(0)

#define SWLOC_ASSERT(x)                         \
do {                                            \
        if (!(x))                               \
                assert(x);                      \
} while(0)

#define SWLOC_PRINT_MSG(msg, ...)               \
do {                                            \
        fprintf(stderr, "\n[swloc][%s] " msg "\n\n", __func__, ## __VA_ARGS__);         \
} while(0)

#endif /* SWLOC_INTERNAL_UTILS_H */


