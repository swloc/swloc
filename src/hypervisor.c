
#include <stdlib.h>
#include <string.h>
#include <sys/queue.h>

#include <swloc.h>

#include "core.h"
#include "context.h"
#include "hypervisor.h"
#include "hypervisor_eager.h"
#include "utils.h"
#include "utlist.h"

int swloc_hypervisor_track_context(struct swloc_hypervisor * hp, swloc_context_t context)
{
        struct swloc_hypervisor_context_track * track = malloc(sizeof(*track));
        SWLOC_ASSERT(track != NULL);

        track->context = context;

        pthread_mutex_lock(&hp->mutex);
        DL_PREPEND(hp->contexts, track);
        pthread_mutex_unlock(&hp->mutex);

        return 0;
}

int swloc_hypervisor_untrack_context(struct swloc_hypervisor * hp, swloc_context_t context)
{
        struct swloc_hypervisor_context_track * track, *tmp;
        pthread_mutex_lock(&hp->mutex);
        DL_FOREACH_SAFE(hp->contexts, track, tmp)
        {
                if (track->context == context)
                {
                        DL_DELETE(hp->contexts, track);
                        free(track);
                        pthread_mutex_unlock(&hp->mutex);
                        return 0;
                }
        }        
        pthread_mutex_unlock(&hp->mutex);

        return -1;
}

static void * swloc_hypervisor_launcher(void * arg)
{
        struct swloc_hypervisor * hp = (struct swloc_hypervisor *) arg;
	return hp->func_run(hp);
}

static struct swloc_hypervisor * swloc_hypervisor_internal_create(enum swloc_hypervisor_type type)
{
        struct swloc_hypervisor * hp = malloc(sizeof(*hp));
        SWLOC_ASSERT_MSG(hp != NULL, "Hypervisor can't be initialized");

        switch (type)
        {
                case SWLOC_HYPERVISOR_TYPE_DEFAULT:
                {
			hp->func_run = swloc_hypervisor_eager;
                        break;
                }

                default :
                        SWLOC_ASSERT_MSG(0, "Type not recognized (%u)", type);

        }

        hp->contexts = NULL;
        hp->run = 1;
        pthread_mutex_init(&hp->mutex, NULL);

        int res = SWLOC_REAL_PTHREAD_CREATE(&hp->thread, NULL, swloc_hypervisor_launcher, (void *) hp);
        SWLOC_ASSERT_MSG(res == 0, "swloc_hypervisor_create can not launch the internal thread (res = %d)", res);

        return hp;
}

int swloc_hypervisor_destroy(struct swloc_hypervisor * hp)
{
        struct swloc_hypervisor_context_track * elt;
        unsigned count = 0;
        DL_COUNT(hp->contexts, elt, count);
        SWLOC_ASSERT(count == 0);

        pthread_mutex_destroy(&hp->mutex);
        hp->run = 0;
        int res = pthread_join(hp->thread, NULL);
        SWLOC_ASSERT_MSG(res == 0, "swloc_hypervisor_destroy can not wait the internal thread");

        free(hp);

        return 0;
}

int swloc_hypervisor_create(enum swloc_hypervisor_type type)
{
        SWLOC_ASSERT_MSG(swloc_core_is_initialized(), "You need to initialize SWLOC before creating a hypervisor");

	swloc_context_t global_context = swloc_core_get_context();

        if (global_context->hp == NULL)
                global_context->hp = swloc_hypervisor_internal_create(type);

        return 0;
}

enum swloc_hypervisor_type swloc_hypervisor_convert_type(char * str)
{
        if (strcmp(str, "DEFAULT") == 0)
                return SWLOC_HYPERVISOR_TYPE_DEFAULT;

        SWLOC_ASSERT_MSG(0, "This hypervisor type is not recognized (%s)", str);
}
