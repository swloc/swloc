#ifndef SWLOC_INTERNAL_CORE_H
#define SWLOC_INTERNAL_CORE_H

#include <dlfcn.h>
#include <hwloc.h>
#include <pthread.h>

#include <swloc.h>

#include "utils.h"

typedef int (*real_hwloc_topology_load) (hwloc_topology_t topology);
#define SWLOC_REAL_HWLOC_TOPOLOGY_LOAD(topology) \
({                                                    \
        real_hwloc_topology_load r_topology_load;    \
        r_topology_load = (real_hwloc_topology_load) dlsym(RTLD_NEXT, "hwloc_topology_load");        \
        SWLOC_ASSERT_MSG(r_topology_load != NULL, "hwloc_topology_load is not found");               \
        /* return value */                              \
        r_topology_load(topology);                   \
})

typedef int (*real_pthread_create) (pthread_t *thread, const pthread_attr_t *attr, void *(*start_routine) (void *), void *arg);

#define SWLOC_REAL_PTHREAD_CREATE(thread, attr, start_routine, arg) \
({                                                    \
        real_pthread_create r_pthread_create;            \
        r_pthread_create = (real_pthread_create) dlsym(RTLD_NEXT, "pthread_create");        \
        SWLOC_ASSERT_MSG(r_pthread_create != NULL, "pthread_create is not found");               \
        /* return value */                              \
        r_pthread_create(thread, attr, start_routine, arg);                   \
})


extern pthread_mutex_t swloc_core_mutex;

unsigned swloc_core_cpu_global();

void swloc_core_set_context(swloc_context_t ctx);

int swloc_core_is_initialized();

hwloc_topology_t swloc_core_get_hwloc_topology();

#endif /* SWLOC_INTERNAL_CORE_H */

