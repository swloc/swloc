#ifndef SWLOC_TBB_H
#define SWLOC_TBB_H

#ifdef __cplusplus 
extern "C"
{
#endif

	int swloc_tbb_launch_kernel(struct swloc_kernel_arg * arg);

#ifdef __cplusplus 
}
#endif

#endif /* SWLOC_TBB_H */
