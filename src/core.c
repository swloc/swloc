
#include <stdlib.h>

#include "core.h"
#include "context.h"
#include "hypervisor.h"
#include "kernel.h"
#include "utils.h"

int swloc_core_init_status = 0;
static hwloc_topology_t global_topology;
pthread_mutex_t swloc_core_mutex = PTHREAD_MUTEX_INITIALIZER;

static pthread_key_t swloc_kernel_ctx;

/* Only used in kernels by users... */
swloc_context_t swloc_core_get_context()
{
        swloc_context_t ctx = (swloc_context_t) pthread_getspecific(swloc_kernel_ctx);

        return ctx;
}

void swloc_core_set_context(swloc_context_t ctx)
{
        int res = pthread_setspecific(swloc_kernel_ctx, ctx);
        SWLOC_ASSERT(res == 0);
}

int swloc_init()
{
        pthread_mutex_lock(&swloc_core_mutex);
        swloc_core_init_status++;
        //Already initialized ?
        if(swloc_core_init_status > 1)
        {
                pthread_mutex_unlock(&swloc_core_mutex);
                return 0;
        }

        pthread_key_create(&swloc_kernel_ctx, NULL);

        hwloc_topology_init(&global_topology);
        hwloc_topology_load(global_topology);

        unsigned nb_cores = hwloc_get_nbobjs_by_type(global_topology, HWLOC_OBJ_CORE);
        swloc_context_t global_context = swloc_context_create(SWLOC_CONTEXT_NB_CPUS, nb_cores, SWLOC_CONTEXT_END);
        swloc_core_set_context(global_context);

        char * env;
        if ((env = getenv("SWLOC_HYPERVISOR")) != NULL)
        {
                swloc_hypervisor_create(swloc_hypervisor_convert_type(env));
        }

        pthread_mutex_unlock(&swloc_core_mutex);

        return 0;
}



int swloc_finalize()
{
        pthread_mutex_lock(&swloc_core_mutex);
        swloc_core_init_status--;
        if (swloc_core_init_status > 0)
        {
                pthread_mutex_unlock(&swloc_core_mutex);
                return 0;
        }

        swloc_context_t global_context = swloc_core_get_context();

        if (global_context->hp)
	{
                swloc_hypervisor_destroy(global_context->hp);
		global_context->hp = NULL;
	}


        swloc_context_destroy(global_context);

        pthread_key_delete(swloc_kernel_ctx);
        
        hwloc_topology_destroy(global_topology);

        pthread_mutex_unlock(&swloc_core_mutex);

        return 0;
}

int swloc_core_is_initialized()
{
	return swloc_core_init_status >= 1;
}

hwloc_topology_t swloc_core_get_hwloc_topology()
{
        return global_topology;
}

unsigned swloc_core_cpu_global()
{
        unsigned nb_cores = hwloc_get_nbobjs_by_type(global_topology, HWLOC_OBJ_CORE);

        return nb_cores;
}

