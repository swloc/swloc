#ifndef SWLOC_KERNEL_INTERNAL_H
#define SWLOC_KERNEL_INTERNAL_H

#include <pthread.h>

#include <swloc_kernel.h>

struct swloc_kernel
{
        pthread_t thread;
        swloc_context_t ctx;
        struct swloc_kernel_options * options;
};

//TODO : may remove that with swloc_kernel
struct swloc_kernel_arg
{
        struct swloc_kernel_options * options;
        swloc_context_t ctx;
};

#ifdef SWLOC_HAVE_OPENMP
void swloc_kernel_omp_set_thread(swloc_context_t ctx);
#endif

void swloc_kernel_internal_init();
void swloc_kernel_internal_finalize();

void swloc_kernel_internal_bind_threads(swloc_context_t ctx, int rank, unsigned internal);

#endif /* SWLOC_KERNEL_INTERNAL_H */

