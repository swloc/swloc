

#include <swloc.h>

#include "context.h"
#include "hypervisor.h"
#include "hypervisor_eager.h"
#include "kernel.h"
#include "utils.h"
#include "utlist.h"

void * swloc_hypervisor_eager(struct swloc_hypervisor * hp)
{
        while(hp->run)
        {
                pthread_mutex_lock(&hp->mutex);
                struct swloc_hypervisor_context_track * elt;
                unsigned count = 0;
                DL_COUNT(hp->contexts, elt, count);
// TODO
                if (count > 0)
                {
                        struct swloc_hypervisor_context_track * track, *tmp;
                        
                        /* Check if there are some contexts which are reclaiming cores 
                         * And give them cores */
                        DL_FOREACH_SAFE(hp->contexts, track, tmp)
                        {
                                swloc_context_t ctx = track->context;
                                pthread_mutex_lock(&ctx->father_ctx->mutex);
                                pthread_mutex_lock(&ctx->mutex);

                                if (ctx->father_ctx->status == SWLOC_CONTEXT_STATUS_RECLAIM)
                                {
                                        /* Give back cores to the father */
                                        int cores_removable =  ctx->nb_cores_avail - ctx->nb_cores_min;
                                        if (cores_removable > 0)
                                                swloc_context_modify_cores(ctx, ctx->knl, -cores_removable);
                                }

                                if (ctx->nb_cores_avail < ctx->nb_cores_min)
                                {
                                        int cores_removable = ctx->father_ctx->nb_cores_avail;
                                        int cores_needed = ctx->nb_cores_min - ctx->nb_cores_avail;

                                        if (cores_needed <= cores_removable)
                                        {
                                                swloc_context_modify_cores(ctx, ctx->knl, cores_needed);

						if (ctx->father_ctx->status == SWLOC_CONTEXT_STATUS_RECLAIM)
						{
							ctx->father_ctx->nb_ctx_sons_reclaim--;

							if(ctx->father_ctx->nb_ctx_sons_reclaim == 0)
							{
								ctx->father_ctx->status = SWLOC_CONTEXT_STATUS_RUNNING;
								pthread_cond_broadcast(&ctx->cond);
							}
						}
                                        } 
                                        else
                                        {
                                                //In case the father don't have enough cores 
                                                //First, we add all available cores
                                                if (cores_removable > 0)
                                                        swloc_context_modify_cores(ctx, ctx->knl, cores_removable);

                                                ctx->father_ctx->nb_ctx_sons_reclaim++;
                                                ctx->father_ctx->status = SWLOC_CONTEXT_STATUS_RECLAIM;
                                        }
                                }

                                

                                pthread_mutex_unlock(&ctx->mutex);
                                pthread_mutex_unlock(&ctx->father_ctx->mutex);
                        }


                        DL_FOREACH_SAFE(hp->contexts, track, tmp)
                        {
                                swloc_context_t ctx = track->context;
                                pthread_mutex_lock(&ctx->father_ctx->mutex);

                                unsigned cores_avail = ctx->father_ctx->nb_cores_avail;
                                unsigned cores_per_kernel = cores_avail / ctx->father_ctx->nb_ctx_sons;
                                unsigned cores_remainder = cores_avail % ctx->father_ctx->nb_ctx_sons;

                                if (ctx->father_ctx->status != SWLOC_CONTEXT_STATUS_RECLAIM && cores_avail > 0)
                                {
                                        pthread_mutex_lock(&ctx->mutex);

                                        if (ctx->nb_cores_avail < ctx->nb_cores_max)
                                        {
                                                /* Try to give the same number of cores to each context. 
                                                 * Give the remainder to the first contexts */
                                                unsigned cores_add = cores_per_kernel;
                                                if (cores_remainder > 0)
                                                {
                                                        cores_add++;
                                                        cores_remainder--;
                                                }

                                                /* In case of max number of cores reached, increase the remainder with the difference. */
                                                if (ctx->nb_cores_avail + cores_add > ctx->nb_cores_max)
                                                {
                                                        unsigned cores_correct = ctx->nb_cores_max - ctx->nb_cores_avail;
                                                        cores_remainder += cores_add - cores_correct;
                                                        cores_add = cores_correct;
                                                }

                                                //Give them
                                                swloc_context_modify_cores(ctx, ctx->knl, cores_add);
                                        }
                                        else
                                        {
                                                /* Put useless cores in remainder */
                                                cores_remainder += cores_per_kernel;
                                        }
                                        pthread_mutex_unlock(&ctx->mutex);
                                }
                                
                                pthread_mutex_unlock(&ctx->father_ctx->mutex);
                        }        
                }

                pthread_mutex_unlock(&hp->mutex);

                sleep(2);
        }

        return NULL;
}
