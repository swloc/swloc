#ifdef SWLOC_HAVE_TBB

#include <iostream>
#include <atomic>
#define TBB_PREVIEW_LOCAL_OBSERVER 1
#include <tbb/tbb.h>

#include <swloc.h>

extern "C"
{
#include "context.h"
#include "core.h"
#include "kernel.h"
#include "tbb.h"

int swloc_tbb_launch_kernel(struct swloc_kernel_arg * arg);
}

class swloc_tbb_observer: public tbb::task_scheduler_observer
{
	std::atomic<int> ncpus;
	swloc_context_t ctx;

public:

	swloc_tbb_observer(swloc_context_t ctx_obs, tbb::task_arena &a)
		: tbb::task_scheduler_observer(a)
	{
		ncpus = 0;
		ctx = ctx_obs;
	}

	void on_scheduler_entry( bool )
	{
		int id = std::atomic_fetch_add(&ncpus, 1);
		swloc_kernel_internal_bind_threads(ctx, id, 1);
	}

	void on_scheduler_exit( bool )
	{ 
		int id = std::atomic_fetch_sub(&ncpus, 1);
	}
};

int swloc_tbb_launch_kernel(struct swloc_kernel_arg * arg)
{
	/* Set number of threads in TBB */
	tbb::task_arena arena(arg->ctx->nb_cores_avail);

	/* Use a task scheduler observer to bind the threads */
	swloc_tbb_observer observer(arg->ctx, arena);
	observer.observe(true);

	int res;
	arena.execute([&]{
		res = arg->options->func_main(arg->options->argc, arg->options->argv); 
	});

	observer.observe(false);

	return res;
}

#endif
