#include <omp.h>
#include <hwloc.h>
#include <sched.h>
#include <stdlib.h>
#include <stdio.h>

#include <swloc.h>

#ifdef SWLOC_HAVE_OPENMP
int func(int argc, char ** argv)
{
	(void) argc;
	(void) argv;

        int fail = 0;

	hwloc_topology_t topology;
	hwloc_topology_init(&topology);
	hwloc_topology_load(topology);

        swloc_context_t ctx = swloc_core_get_context();
        hwloc_bitmap_t ctx_set = swloc_context_get_cores(ctx);

        #pragma omp parallel shared(fail)
        {
                #pragma omp master
                fprintf(stderr, "number of threads : %d\n", omp_get_num_threads());

                hwloc_bitmap_t set = hwloc_bitmap_alloc();
                hwloc_bitmap_zero(set);
		hwloc_get_cpubind (topology, set, HWLOC_CPUBIND_THREAD);

		#pragma omp critical
		{
			if (! hwloc_bitmap_isincluded(set, ctx_set))
			{
				fprintf(stderr, "SWLOC binding core failed ! \n");
				fail = 1;
			}

			//remove set to ctx_set
			hwloc_bitmap_xor(ctx_set, ctx_set, set);
		}

                hwloc_bitmap_free(set);

        }

	hwloc_topology_destroy(topology);

        if(! hwloc_bitmap_iszero(ctx_set))
        {
                fprintf(stderr, "SWLOC does not provide work to each core ! \n");
                fail = 1;
        }

        hwloc_bitmap_free(ctx_set);
       
        if (fail)
                return EXIT_FAILURE;
        else
                return EXIT_SUCCESS;
}
#endif

int main(int argc, char ** argv)
{
#ifdef SWLOC_HAVE_OPENMP
        swloc_init();

        hwloc_topology_t topology;
        hwloc_topology_init(&topology);
        hwloc_topology_load(topology);

	int maxcores = hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_CORE);
                
        swloc_context_t ctx1 = swloc_context_create(SWLOC_CONTEXT_NB_CPUS, maxcores/2, SWLOC_CONTEXT_OMP_SUPPORT, SWLOC_CONTEXT_END);
        swloc_context_t ctx2 = swloc_context_create(SWLOC_CONTEXT_NB_CPUS, maxcores/2, SWLOC_CONTEXT_OMP_SUPPORT, SWLOC_CONTEXT_END);

        struct swloc_kernel_options opt;
        swloc_kernel_options_init(&opt);
        opt.func_main = func;
        opt.argc = argc;
        opt.argv = argv;

        swloc_kernel_t knl1 = swloc_kernel_start(ctx1, &opt);
        swloc_kernel_t knl2 = swloc_kernel_start(ctx2, &opt);

        swloc_kernel_wait(knl1);
        swloc_kernel_wait(knl2);

        swloc_context_destroy(ctx1);
        swloc_context_destroy(ctx2);

        hwloc_topology_destroy(topology);

        swloc_finalize();
#else
        abort();
#endif

        return EXIT_SUCCESS;
}
