#include <hwloc.h>
#include <stdlib.h>
#include <stdio.h>

#include <swloc.h>

int func(int argc, char ** argv)
{
	(void) argc;
	(void) argv;

	int fail = 0;

        hwloc_topology_t topology;
        hwloc_topology_init(&topology);
        hwloc_topology_load(topology);

        swloc_context_t ctx = swloc_core_get_context();
        hwloc_bitmap_t ctx_set = swloc_context_get_cores(ctx);

	hwloc_bitmap_t set = hwloc_bitmap_alloc();
	hwloc_bitmap_zero(set);

	int nb_cores = hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_CORE);
	assert(nb_cores >= 0);

	unsigned i;
	for (i = 0; i < (unsigned)nb_cores; i++)
	{
		hwloc_obj_t obj = hwloc_get_obj_by_type(topology, HWLOC_OBJ_CORE, i);
		hwloc_bitmap_or(set, obj->cpuset, set);
	}

	if (! hwloc_bitmap_isequal(set, ctx_set))
	{
		fprintf(stderr, "SWLOC checking hwloc cores failed ! \n");
		fail = 1;
	}

	hwloc_bitmap_free(set);
	hwloc_bitmap_free(ctx_set);

        fprintf(stderr, "Cores available in the context : %u \n", hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_CORE));
        
        hwloc_topology_destroy(topology);

	if (fail)
		return EXIT_FAILURE;
	else
		return EXIT_SUCCESS;
}

int main(int argc, char ** argv)
{
        swloc_init();

        hwloc_topology_t topology;
        hwloc_topology_init(&topology);
        hwloc_topology_load(topology);

	int maxcores = hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_CORE);
	assert(maxcores >= 0);
	
	swloc_policy_set(swloc_policy_scatter);
                
        swloc_context_t ctx1 = swloc_context_create(SWLOC_CONTEXT_NB_CPUS, maxcores/2, SWLOC_CONTEXT_END);
        swloc_context_t ctx2 = swloc_context_create(SWLOC_CONTEXT_NB_CPUS, maxcores/2, SWLOC_CONTEXT_END);

        struct swloc_kernel_options opt;
        swloc_kernel_options_init(&opt);
        opt.func_main = func;
        opt.argc = argc;
        opt.argv = argv;

        swloc_kernel_t knl1 = swloc_kernel_start(ctx1, &opt);
        swloc_kernel_t knl2 = swloc_kernel_start(ctx2, &opt);

        swloc_kernel_wait(knl1);
        swloc_kernel_wait(knl2);

        swloc_context_destroy(ctx1);
        swloc_context_destroy(ctx2);

        hwloc_topology_destroy(topology);

        swloc_finalize();

	return EXIT_SUCCESS;
}
