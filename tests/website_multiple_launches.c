#include <stdlib.h>
#include <stdio.h>

#include <swloc.h>

int func_second_lvl(int argc, char ** argv)
{
        (void) argc;
        (void) argv;

        //Do work

        return EXIT_SUCCESS;
}

int func_first_lvl(int argc, char ** argv)
{
        swloc_init();

        swloc_context_t ctx1 = swloc_context_create(SWLOC_CONTEXT_NB_CPUS_HALF, SWLOC_CONTEXT_END);
        swloc_context_t ctx2 = swloc_context_create(SWLOC_CONTEXT_NB_CPUS_HALF, SWLOC_CONTEXT_END);

        struct swloc_kernel_options opt;
        swloc_kernel_options_init(&opt);
        opt.func_main = func_second_lvl;
        opt.argc = argc;
        opt.argv = argv;

        swloc_kernel_t knl1 = swloc_kernel_start(ctx1, &opt);
        swloc_kernel_t knl2 = swloc_kernel_start(ctx2, &opt);

        swloc_kernel_wait(knl1);
        swloc_kernel_wait(knl2);

        swloc_context_destroy(ctx1);
        swloc_context_destroy(ctx2);

        swloc_finalize();

        return EXIT_SUCCESS;
}

int main(int argc, char ** argv)
{
        swloc_init();

        swloc_context_t ctx1 = swloc_context_create(SWLOC_CONTEXT_NB_CPUS_HALF, SWLOC_CONTEXT_END);
        swloc_context_t ctx2 = swloc_context_create(SWLOC_CONTEXT_NB_CPUS_HALF, SWLOC_CONTEXT_END);

        struct swloc_kernel_options opt;
        swloc_kernel_options_init(&opt);
        opt.func_main = func_first_lvl;
        opt.argc = argc;
        opt.argv = argv;

        swloc_kernel_t knl1 = swloc_kernel_start(ctx1, &opt);
        swloc_kernel_t knl2 = swloc_kernel_start(ctx2, &opt);

        swloc_kernel_wait(knl1);
        swloc_kernel_wait(knl2);

        swloc_context_destroy(ctx1);
        swloc_context_destroy(ctx2);

        swloc_finalize();

        return EXIT_SUCCESS;
}
