#include "tbb/tbb.h"
#include <cstdio>
#include <iostream>

#include <swloc.h>

using namespace tbb;

int func_simple(int arc, char ** argv)
{
	int nDefThreads = tbb::task_scheduler_init::default_num_threads();

        fprintf(stderr, "Cores available in the context (func_simple) : %d \n", nDefThreads);
	
	return EXIT_SUCCESS;
}

int multiple_launch(int argc, char ** argv)
{
	int nDefThreads = tbb::task_scheduler_init::default_num_threads();

        fprintf(stderr, "Cores available in the context (multiple_launch) : %d \n", nDefThreads);

        swloc_context_t ctx1 = swloc_context_create(SWLOC_CONTEXT_NB_CPUS, nDefThreads/2, SWLOC_CONTEXT_TBB_SUPPORT, SWLOC_CONTEXT_END);
        swloc_context_t ctx2 = swloc_context_create(SWLOC_CONTEXT_NB_CPUS, nDefThreads/2, SWLOC_CONTEXT_TBB_SUPPORT, SWLOC_CONTEXT_END);

        struct swloc_kernel_options opt;
        swloc_kernel_options_init(&opt);
        opt.func_main = func_simple;
        opt.argc = argc;
        opt.argv = argv;

        swloc_kernel_t knl1 = swloc_kernel_start(ctx1, &opt);
        swloc_kernel_t knl2 = swloc_kernel_start(ctx2, &opt);

        swloc_kernel_wait(knl1);
        swloc_kernel_wait(knl2);

        swloc_context_destroy(ctx1);
        swloc_context_destroy(ctx2);

}

template <typename R, typename S> 
R tbb_pi( S num_steps )
{
	const R step = R(1) / num_steps;

	return step * tbb::parallel_reduce( tbb::blocked_range<S>( 0, num_steps ), R(0),
		[step] ( const tbb::blocked_range<S> r, R local_sum ) -> R 
		{
			for ( S i = r.begin(); i < r.end(); ++i )
			{
				R x = (i + R(0.5)) * step;
				local_sum += R(4) / (R(1) + x*x);
			}

			return local_sum;
		},
		std::plus<R>()
	);

}

int func(int argc, char ** argv)
{
        hwloc_topology_t topology;
        hwloc_topology_init(&topology);
        hwloc_topology_load(topology);

	tbb::task_scheduler_init init();

	const size_t N = 10L * 1000 * 1000 * 1000;
	const double pi = tbb_pi<double>( N );

	int maxcores = hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_CORE);
        fprintf(stderr, "Cores available in the context (func) : %d \n", maxcores);

	int nDefThreads = tbb::task_scheduler_init::default_num_threads();

	if (nDefThreads != maxcores)
	{
		fprintf(stderr, "Error about number of cores between SwLoc and TBB (%d and %d)\n", maxcores, nDefThreads);
		abort();
	}

	return EXIT_SUCCESS;
}

int main(int argc, char ** argv)
{
        swloc_init();

        hwloc_topology_t topology;
        hwloc_topology_init(&topology);
        hwloc_topology_load(topology);

	int maxcores = hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_CORE);
        fprintf(stderr, "Cores available on the machine : %d \n", maxcores);
                
        swloc_context_t ctx1 = swloc_context_create(SWLOC_CONTEXT_NB_CPUS, maxcores/2, SWLOC_CONTEXT_TBB_SUPPORT, SWLOC_CONTEXT_END);
        swloc_context_t ctx2 = swloc_context_create(SWLOC_CONTEXT_NB_CPUS, maxcores/2, SWLOC_CONTEXT_TBB_SUPPORT, SWLOC_CONTEXT_END);

        struct swloc_kernel_options opt;
        swloc_kernel_options_init(&opt);
        opt.func_main = func;
        opt.argc = argc;
        opt.argv = argv;

        swloc_kernel_t knl1 = swloc_kernel_start(ctx1, &opt);
        swloc_kernel_t knl2 = swloc_kernel_start(ctx2, &opt);

        swloc_kernel_wait(knl1);
        swloc_kernel_wait(knl2);

        opt.func_main = multiple_launch;

        knl1 = swloc_kernel_start(ctx1, &opt);
        knl2 = swloc_kernel_start(ctx2, &opt);

        swloc_kernel_wait(knl1);
        swloc_kernel_wait(knl2);

        swloc_context_destroy(ctx1);
        swloc_context_destroy(ctx2);

        hwloc_topology_destroy(topology);

        swloc_finalize();
	return EXIT_SUCCESS;
}

