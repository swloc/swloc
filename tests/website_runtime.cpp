#include <omp.h>
#include <stdlib.h>
#include "tbb/tbb.h"

#include <swloc.h>

int func_omp(int argc, char ** argv)
{
        (void) argc;
        (void) argv;

        #pragma omp parallel
        {
                printf("I am OMP thread %d / %d\n", omp_get_thread_num(), omp_get_num_threads());
        }

        return EXIT_SUCCESS;
}

int func_tbb(int argc, char ** argv)
{
        (void) argc;
        (void) argv;

        int nThreads = tbb::task_scheduler_init::default_num_threads();
 
        printf("TBB context has %d threads \n", nThreads);	

        return EXIT_SUCCESS;
}

int main(int argc, char ** argv)
{
        swloc_init();
                
        swloc_context_t ctx1 = swloc_context_create(SWLOC_CONTEXT_NB_CPUS_HALF, SWLOC_CONTEXT_OMP_SUPPORT, SWLOC_CONTEXT_END);
        swloc_context_t ctx2 = swloc_context_create(SWLOC_CONTEXT_NB_CPUS_HALF, SWLOC_CONTEXT_TBB_SUPPORT, SWLOC_CONTEXT_END);

        struct swloc_kernel_options opt;
        swloc_kernel_options_init(&opt);
        opt.argc = argc;
        opt.argv = argv;

        opt.func_main = func_omp;
        swloc_kernel_t knl1 = swloc_kernel_start(ctx1, &opt);

        opt.func_main = func_tbb;
        swloc_kernel_t knl2 = swloc_kernel_start(ctx2, &opt);

        swloc_kernel_wait(knl1);
        swloc_kernel_wait(knl2);

        swloc_context_destroy(ctx1);
        swloc_context_destroy(ctx2);

        swloc_finalize();

        return EXIT_SUCCESS;
}
