#include <hwloc.h>
#include <stdlib.h>
#include <stdio.h>

#include <swloc.h>

volatile unsigned received_core = 0;

int handler_second_lvl(swloc_context_t ctx, int cores)
{
	fprintf(stderr, "Second lvl receive %d cores \n", cores);
	swloc_context_ack_cores(ctx);

	received_core = 1;

	return 0;
}

int func_second_lvl(int argc, char ** argv)
{
	(void) argc;
	(void) argv;

	int fail = 0;

        hwloc_topology_t topology;
        hwloc_topology_init(&topology);
        hwloc_topology_load(topology);

        swloc_context_t ctx = swloc_core_get_context();
        hwloc_bitmap_t ctx_set = swloc_context_get_cores(ctx);

	hwloc_bitmap_t set = hwloc_bitmap_alloc();
	hwloc_bitmap_zero(set);

	int nb_cores = hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_CORE);
	assert(nb_cores >= 0);

	unsigned i;
	for (i = 0; i < (unsigned)nb_cores; i++)
	{
		hwloc_obj_t obj = hwloc_get_obj_by_type(topology, HWLOC_OBJ_CORE, i);
		hwloc_bitmap_or(set, obj->cpuset, set);
	}

	if (! hwloc_bitmap_isequal(set, ctx_set))
	{
		fprintf(stderr, "SWLOC checking hwloc cores failed ! \n");
		fail = 1;
	}

	hwloc_bitmap_free(set);
	hwloc_bitmap_free(ctx_set);

        fprintf(stderr, "Cores available in the second lvl : %u \n", hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_CORE));

	while(!received_core)
		;

        hwloc_topology_destroy(topology);

	/* Reload topology to get new core */
        hwloc_topology_init(&topology);
        hwloc_topology_load(topology);

        fprintf(stderr, "Cores available in the second lvl after received core : %u \n", hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_CORE));
        
        hwloc_topology_destroy(topology);

	if (fail)
		return EXIT_FAILURE;
	else
		return EXIT_SUCCESS;
}

int handler_first_lvl(swloc_context_t ctx, int cores)
{
	fprintf(stderr, "First lvl receive %d cores \n", cores);
	swloc_context_ack_cores(ctx);

	return 0;
}

int func_first_lvl(int argc, char ** argv)
{
        swloc_init();

        swloc_hypervisor_create(SWLOC_HYPERVISOR_TYPE_DEFAULT);

	(void) argc;
	(void) argv;

	int fail = 0;

        hwloc_topology_t topology;
        hwloc_topology_init(&topology);
        hwloc_topology_load(topology);
        
	int nb_cores = hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_CORE);
	assert(nb_cores >= 0);

        swloc_context_t ctx = swloc_core_get_context();
        hwloc_bitmap_t ctx_set = swloc_context_get_cores(ctx);

	hwloc_bitmap_t set = hwloc_bitmap_alloc();
	hwloc_bitmap_zero(set);

	unsigned i;
	for (i = 0; i < (unsigned)nb_cores; i++)
	{
		hwloc_obj_t obj = hwloc_get_obj_by_type(topology, HWLOC_OBJ_CORE, i);
		hwloc_bitmap_or(set, obj->cpuset, set);
	}

	if (! hwloc_bitmap_isequal(set, ctx_set))
	{
		fprintf(stderr, "SWLOC checking hwloc cores failed ! \n");
		fail = 1;
	}

	hwloc_bitmap_free(set);
	hwloc_bitmap_free(ctx_set);

        fprintf(stderr, "Cores available in the first lvl : %u \n", hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_CORE));

        swloc_context_t ctx1 = swloc_context_create(SWLOC_CONTEXT_NB_CPUS_MIN, nb_cores/2, SWLOC_CONTEXT_NB_CPUS_MAX, nb_cores/2+1, SWLOC_CONTEXT_END);
        swloc_context_t ctx2 = swloc_context_create(SWLOC_CONTEXT_NB_CPUS_MIN, nb_cores/2, SWLOC_CONTEXT_NB_CPUS_MAX, nb_cores/2+1, SWLOC_CONTEXT_END);

        struct swloc_kernel_options opt;
        swloc_kernel_options_init(&opt);
        opt.func_main = func_second_lvl;
        opt.argc = argc;
        opt.argv = argv;
	opt.handler_modify_cpu_cores = handler_second_lvl;

        swloc_kernel_t knl1 = swloc_kernel_start(ctx1, &opt);
        swloc_kernel_t knl2 = swloc_kernel_start(ctx2, &opt);

        swloc_kernel_wait(knl1);
        swloc_kernel_wait(knl2);

        swloc_context_destroy(ctx1);
        swloc_context_destroy(ctx2);

        hwloc_topology_destroy(topology);

        swloc_finalize();

	if (fail)
		return EXIT_FAILURE;
	else
		return EXIT_SUCCESS;
}

int main(int argc, char ** argv)
{
        swloc_init();

        swloc_hypervisor_create(SWLOC_HYPERVISOR_TYPE_DEFAULT);

        hwloc_topology_t topology;
        hwloc_topology_init(&topology);
        hwloc_topology_load(topology);

	int maxcores = hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_CORE);

        if (maxcores < 4)
        {
                fprintf(stderr, "Only %d cores are available (need 4), Skip this test !\n", maxcores);
                return 77;
        }

	unsigned num_cores = maxcores/2;
                
        swloc_context_t ctx1 = swloc_context_create(SWLOC_CONTEXT_NB_CPUS, num_cores, SWLOC_CONTEXT_END);
        swloc_context_t ctx2 = swloc_context_create(SWLOC_CONTEXT_NB_CPUS, (num_cores > 3) ? num_cores-1 : num_cores, SWLOC_CONTEXT_END);

        struct swloc_kernel_options opt;
        swloc_kernel_options_init(&opt);
        opt.func_main = func_first_lvl;
        opt.argc = argc;
        opt.argv = argv;
	opt.handler_modify_cpu_cores = handler_first_lvl;

        swloc_kernel_t knl1 = swloc_kernel_start(ctx1, &opt);
        swloc_kernel_t knl2 = swloc_kernel_start(ctx2, &opt);

        swloc_kernel_wait(knl1);
        swloc_kernel_wait(knl2);

        swloc_context_destroy(ctx1);
        swloc_context_destroy(ctx2);

        hwloc_topology_destroy(topology);

        swloc_finalize();

        return EXIT_SUCCESS;
}
