#include <hwloc.h>
#include <stdlib.h>
#include <stdio.h>

#include <swloc.h>

unsigned maxcores = 0;
unsigned begincores = 1;
volatile unsigned max_cores_add_reached = 0;
volatile unsigned max_cores_del_reached = 0;

int handle_cpu_cores_add(swloc_context_t ctx, int cores)
{
        fprintf(stderr, "Handle_ADD requested %d cores \n", cores);

        hwloc_topology_t topology;
        hwloc_topology_init(&topology);
        hwloc_topology_load(topology);

        unsigned localcores = hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_CORE);

        hwloc_topology_destroy(topology);

        swloc_context_ack_cores(ctx);

        if (localcores >= maxcores)
                max_cores_add_reached = 1;

        return 0;
}

int func_add(int argc, char ** argv)
{
	(void) argc;
	(void) argv;

        int fail = 0;

        hwloc_topology_t topology;
        hwloc_topology_init(&topology);
        hwloc_topology_load(topology);

        unsigned localcores = hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_CORE);

        hwloc_topology_destroy(topology);

        if (localcores >= maxcores)
                max_cores_add_reached = 1;

        while(!max_cores_add_reached)
                ;

        hwloc_topology_init(&topology);
        hwloc_topology_load(topology);

        swloc_context_t ctx = swloc_core_get_context();
        hwloc_bitmap_t ctx_set = swloc_context_get_cores(ctx);

	hwloc_bitmap_t set = hwloc_bitmap_alloc();
	hwloc_bitmap_zero(set);

	int nb_cores = hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_CORE);
	assert(nb_cores >= 0);

	unsigned i;
	for (i = 0; i < (unsigned)nb_cores; i++)
	{
		hwloc_obj_t obj = hwloc_get_obj_by_type(topology, HWLOC_OBJ_CORE, i);
		hwloc_bitmap_or(set, obj->cpuset, set);
	}

	if (! hwloc_bitmap_isequal(set, ctx_set))
	{
		fprintf(stderr, "SWLOC checking hwloc cores failed ! \n");
		fail = 1;
	}

	hwloc_bitmap_free(set);
	hwloc_bitmap_free(ctx_set);

        fprintf(stderr, "Context ADD : Cores at the beginning : %d \n", begincores);
        fprintf(stderr, "Context ADD : Cores to reach : %d \n", maxcores);
        fprintf(stderr, "Context ADD : Cores available in the context: %d \n", nb_cores);
        
        hwloc_topology_destroy(topology);

        if (fail)
        {
                fprintf(stderr, "Context ADD : Failure ! \n");
                return EXIT_FAILURE;
        }
        else
        {
                fprintf(stderr, "Context ADD : Success ! \n");
                return EXIT_SUCCESS;
        }
}

int handle_cpu_cores_del(swloc_context_t ctx, int cores)
{
        fprintf(stderr, "Handle_DEL requested %d cores \n", cores);

        hwloc_topology_t topology;
        hwloc_topology_init(&topology);
        hwloc_topology_load(topology);

        unsigned localcores = hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_CORE);

        hwloc_topology_destroy(topology);

        swloc_context_ack_cores(ctx);

        if (localcores >= maxcores-begincores)
                max_cores_del_reached = 1;

        return 0;

}

int func_del(int argc, char ** argv)
{
	(void) argc;
	(void) argv;

        int fail = 0;

        hwloc_topology_t topology;
        hwloc_topology_init(&topology);
        hwloc_topology_load(topology);

        unsigned localcores = hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_CORE);

        hwloc_topology_destroy(topology);

        if (localcores >= maxcores-begincores)
                max_cores_del_reached = 1;

        while(!max_cores_del_reached)
                ;

        hwloc_topology_init(&topology);
        hwloc_topology_load(topology);

        swloc_context_t ctx = swloc_core_get_context();
        hwloc_bitmap_t ctx_set = swloc_context_get_cores(ctx);

	hwloc_bitmap_t set = hwloc_bitmap_alloc();
	hwloc_bitmap_zero(set);

	int nb_cores = hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_CORE);
	assert(nb_cores >= 0);

	unsigned i;
	for (i = 0; i < (unsigned)nb_cores; i++)
	{
		hwloc_obj_t obj = hwloc_get_obj_by_type(topology, HWLOC_OBJ_CORE, i);
		hwloc_bitmap_or(set, obj->cpuset, set);
	}

	if (! hwloc_bitmap_isequal(set, ctx_set))
	{
		fprintf(stderr, "SWLOC checking hwloc cores failed ! \n");
		fail = 1;
	}

	hwloc_bitmap_free(set);
	hwloc_bitmap_free(ctx_set);

        fprintf(stderr, "Context DEL : Cores to reach : %d \n", maxcores-begincores);
        fprintf(stderr, "Context DEL : Cores available in the context: %d \n", nb_cores);
        
        hwloc_topology_destroy(topology);

        if (fail)
        {
                fprintf(stderr, "Context DEL : Failure ! \n");
                return EXIT_FAILURE;
        }
        else
        {
                fprintf(stderr, "Context DEL : Success ! \n");
                return EXIT_SUCCESS;
        }
}

int main(int argc, char ** argv)
{
        swloc_init();

        swloc_hypervisor_create(SWLOC_HYPERVISOR_TYPE_DEFAULT);

        hwloc_topology_t topology;
        hwloc_topology_init(&topology);
        hwloc_topology_load(topology);

        maxcores = hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_CORE);
                
        swloc_context_t ctx1 = swloc_context_create(SWLOC_CONTEXT_NB_CPUS_MIN, begincores, SWLOC_CONTEXT_NB_CPUS_MAX, maxcores, SWLOC_CONTEXT_END);

        struct swloc_kernel_options opt;
        swloc_kernel_options_init(&opt);
        opt.func_main = func_add;
        opt.argc = argc;
        opt.argv = argv;
        opt.handler_modify_cpu_cores = handle_cpu_cores_add;

        swloc_kernel_t knl1 = swloc_kernel_start(ctx1, &opt);

        swloc_kernel_wait(knl1);

        swloc_context_t ctx2 = swloc_context_create(SWLOC_CONTEXT_NB_CPUS, maxcores-begincores, SWLOC_CONTEXT_END);

        opt.func_main = func_del;
        opt.argc = argc;
        opt.argv = argv;
        opt.handler_modify_cpu_cores = handle_cpu_cores_del;

        swloc_kernel_t knl2 = swloc_kernel_start(ctx2, &opt);

        swloc_kernel_wait(knl2);

        swloc_context_destroy(ctx1);
        swloc_context_destroy(ctx2);

        hwloc_topology_destroy(topology);

        swloc_finalize();

        return EXIT_SUCCESS;
}
