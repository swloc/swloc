#include <hwloc.h>
#include <stdlib.h>
#include <stdio.h>

#include <swloc.h>

void * thread_check(void * arg)
{
        swloc_context_t * received_ctx = (swloc_context_t *) arg;

        swloc_context_t ctx = swloc_core_get_context();

        if (*received_ctx != ctx)
        {
                fprintf(stderr, "SwLoc contexts are not the same (%p and %p)\n", *received_ctx, ctx);
                abort();
        }

        hwloc_topology_t topology;
        hwloc_topology_init(&topology);
        hwloc_topology_load(topology);

        hwloc_bitmap_t ctx_set = swloc_context_get_cores(ctx); 

        hwloc_bitmap_t set = hwloc_bitmap_alloc();
        hwloc_bitmap_zero(set);
        hwloc_get_cpubind (topology, set, HWLOC_CPUBIND_THREAD);

        if (! hwloc_bitmap_isincluded(set, ctx_set))
        {
                fprintf(stderr, "SWLOC binding core failed ! \n");
                abort();
        }

        hwloc_bitmap_free(set);
        hwloc_bitmap_free(ctx_set);
	hwloc_topology_destroy(topology);

        return NULL;
}

int func(int argc, char ** argv)
{
	(void) argc;
	(void) argv;

        hwloc_topology_t topology;
        hwloc_topology_init(&topology);
        hwloc_topology_load(topology);

        swloc_context_t ctx = swloc_core_get_context();

	int nb_cores = hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_CORE);
	assert(nb_cores >= 0);

        pthread_t threads[nb_cores];

	unsigned i;
	for (i = 0; i < (unsigned)nb_cores; i++)
	{
                pthread_create(&threads[i], NULL, thread_check, (void *) &ctx);
	}

	for (i = 0; i < (unsigned)nb_cores; i++)
	{
                pthread_join(threads[i], NULL);
	}
        
        hwloc_topology_destroy(topology);

        return EXIT_SUCCESS;
}

int main(int argc, char ** argv)
{
        swloc_init();

        hwloc_topology_t topology;
        hwloc_topology_init(&topology);
        hwloc_topology_load(topology);

	int maxcores = hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_CORE);
                
        swloc_context_t ctx1 = swloc_context_create(SWLOC_CONTEXT_NB_CPUS, maxcores/2, SWLOC_CONTEXT_END);
        swloc_context_t ctx2 = swloc_context_create(SWLOC_CONTEXT_NB_CPUS, maxcores/2, SWLOC_CONTEXT_END);

        struct swloc_kernel_options opt;
        swloc_kernel_options_init(&opt);
        opt.func_main = func;
        opt.argc = argc;
        opt.argv = argv;

        swloc_kernel_t knl1 = swloc_kernel_start(ctx1, &opt);
        swloc_kernel_t knl2 = swloc_kernel_start(ctx2, &opt);

        swloc_kernel_wait(knl1);
        swloc_kernel_wait(knl2);

        swloc_context_destroy(ctx1);
        swloc_context_destroy(ctx2);

        hwloc_topology_destroy(topology);

        swloc_finalize();

        return EXIT_SUCCESS;
}
