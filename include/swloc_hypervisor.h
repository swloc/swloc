#ifndef SWLOC_HYPERVISOR_H
#define SWLOC_HYPERVISOR_H

/**
 * \typedef int (*swloc_hypervisor_handler_modify_cpu_cores) (swloc_context_t ctx, int cores)
 * \brief A pointer to a handler called by the hypervisor when it decides to modify the number of CPU cores allocated to the context.
 * \param cores Number of cores requested.
 * \param ctx Context modified.
 */
typedef int (*swloc_hypervisor_handler_modify_cpu_cores) (swloc_context_t ctx, int cores);

/**
 * \enum swloc_hypervisor_type
 * \brief This enumeration contains all available parameters for the function swloc_hypervisor_create().
 */
enum swloc_hypervisor_type
{
        /**
         * \brief Default hypervisor.
         */
        SWLOC_HYPERVISOR_TYPE_DEFAULT
};


/**
 * int swloc_hypervisor_create(enum swloc_hypervisor_type type)
 * \brief Create an hypervisor automatically destroyed in swloc_finalize().
 * \param type Specify which type of hypervisor to create.
 */
int swloc_hypervisor_create(enum swloc_hypervisor_type type);

#endif /* SWLOC_HYPERVISOR_H */ 
