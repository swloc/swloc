#ifndef SWLOC_CONTEXT_H
#define SWLOC_CONTEXT_H

#include <hwloc.h>
#include <stdarg.h>

/**
 * \struct swloc_context
 * \brief This structure represents a context in the SwLoc library.
 */
struct swloc_context;

/**
 * \typedef struct swloc_context * swloc_context_t
 * \brief A pointer to a struct swloc_context.
 */
typedef struct swloc_context * swloc_context_t;

/**
 * \enum swloc_context_type
 * \brief This enumeration contains all available parameters for the function swloc_context_create().
 */
enum swloc_context_type
{
        /**
         * \brief Parameter to define how many CPU cores are requested to create the context.
         * It's equivalent to set SWLOC_CONTEXT_NB_CPUS_MIN and SWLOC_CONTEXT_NB_CPUS_MAX with the same number.
         * You need to put an unsigned int after SWLOC_CONTEXT_NB_CPUS to define the number of CPU cores.
         */
        SWLOC_CONTEXT_NB_CPUS,

        /**
         * \brief Parameter to define the minimal number of CPU cores.
         * You need to put an unsigned int after SWLOC_CONTEXT_NB_CPUS_MIN to define the number of CPU cores.
         */
        SWLOC_CONTEXT_NB_CPUS_MIN,

        /**
         * \brief Parameter to define the maximal number of CPU cores.
         * You need to put an unsigned int after SWLOC_CONTEXT_NB_CPUS_MAX to define the number of CPU cores.
         */
        SWLOC_CONTEXT_NB_CPUS_MAX,

        /**
         * \brief Parameter to define the number of CPU cores at the beginning of the context.
         * You need to put an unsigned int after SWLOC_CONTEXT_NB_CPUS_START to define the number of CPU cores.
         * If not provided, the value of SWLOC_CONTEXT_NB_CPUS_MIN is used.
         */
        SWLOC_CONTEXT_NB_CPUS_START,
        
        /**
         * \brief Parameter to request the half of number CPU cores available to create the context.
         */
        SWLOC_CONTEXT_NB_CPUS_HALF,

        /**
         * \brief Parameter to request the quarter of number CPU cores available to create the context.
         */
        SWLOC_CONTEXT_NB_CPUS_QUARTER,

        /**
         * \brief Parameter to request the three quarters of number CPU cores available to create the context.
         */
        SWLOC_CONTEXT_NB_CPUS_THREE_QUARTERS,

        /**
         * \brief Parameter to enable the support of OpenMP in the context.
         */
        SWLOC_CONTEXT_OMP_SUPPORT,

        /**
         * \brief Parameter to enable the support of Intel TBB in the context.
         */
        SWLOC_CONTEXT_TBB_SUPPORT,

        /**
         * \brief Parameter to specify the end of the list of arguments.
         */
        SWLOC_CONTEXT_END        
};


/**
 * \fn swloc_context_t swloc_context_create(enum swloc_context_type type,...)
 * \brief Create a context in SwLoc.
 *
 * Use enum ::swloc_context_type to specify the resources available for the new context.
 * Add ::SWLOC_CONTEXT_END to the end of the list.
 */
swloc_context_t swloc_context_create(enum swloc_context_type type,...);


/**
 * \fn int swloc_context_destroy(swloc_context_t ctx)
 * \brief Destroy the context.
 * \param ctx Context to destroy.
 *
 * Release reserved resources from context \p ctx.
 */
int swloc_context_destroy(swloc_context_t ctx);

/**
 * \fn hwloc_bitmap_t swloc_context_get_cores(swloc_context_t ctx)
 * \brief Get a bitmap of hwloc logic cores allocated in the context \p ctx.
 * \param ctx Specify the context to get cores to make the bitmap.
 */
hwloc_bitmap_t swloc_context_get_cores(swloc_context_t ctx);

/**
 * \fn void swloc_context_ack_cores(swloc_context_t ctx)
 * \brief Call this function when your handler is notified by the hypervisor to acknowledge the number of cores given.
 * \param ctx Specify the context where cores are to acknoledge.
 *
 */
void swloc_context_ack_cores(swloc_context_t ctx);

#endif /* SWLOC_CONTEXT_H */
