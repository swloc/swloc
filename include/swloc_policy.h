#ifndef SWLOC_POLICY_H
#define SWLOC_POLICY_H

/**
 * \enum swloc_policy
 * \brief This enumeration contains all available policies to distribute cores to kernels.
 */
enum swloc_policy
{
	/**
	 * \brief Policy provides the next available core given by user in an array.
	 */
	swloc_policy_explicit,

	/**
         * \brief Policy provides the next available core indexed by hwloc logical index.
         */
        swloc_policy_next,

        /**
         * \brief Policy provides the next available core on the same socket.
         */
        swloc_policy_socket,

        /**
         * \brief Policy scatters cores across the sockets.
         */
	swloc_policy_scatter
};

/**
 * \fn int swloc_policy_set(enum swloc_core_policy policy, ...)
 * \brief Function to set the policy with optional parameters.
 */
int swloc_policy_set(enum swloc_policy policy, ...);

/**
 * \fn enum swloc_policy swloc_core_policy_get()
 * \brief Function to get the policy.
 */
enum swloc_policy swloc_core_policy_get();

#endif /* SWLOC_POLICY_H */
