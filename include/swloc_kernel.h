#ifndef SWLOC_KERNEL_H
#define SWLOC_KERNEL_H

#include <swloc_context.h>
#include <swloc_hypervisor.h>

/**
 * \struct swloc_kernel
 * \brief This structure represents a kernel in the SwLoc library.
 */
struct swloc_kernel;

/**
 * \typedef struct swloc_kernel * swloc_kernel_t
 * \brief A pointer to a struct swloc_kernel.
 */
typedef struct swloc_kernel * swloc_kernel_t;

/**
 * \typedef int (*swloc_kernel_function_main) (int argc, char ** argv)
 * \brief A pointer to a main kernel function.
 */
typedef int (*swloc_kernel_function_main) (int argc, char ** argv);

/**
 * \struct swloc_kernel_options
 * \brief This structure contains options to launch kernels.
 */
struct swloc_kernel_options
{
        /**
         * \brief Pointer to the main function of the kernel.
         */
        swloc_kernel_function_main func_main;

        /**
         * \brief Number of arguments in argv.
         */
        int argc;

        /**
         * \brief Arguments for func_main function.
         */
        char ** argv;

        /**
         * \brief Specify which handler is called when a core is added/removed.
         *
         *  This function has to call ::swloc_context_ack_cores at the end.
         */
        swloc_hypervisor_handler_modify_cpu_cores handler_modify_cpu_cores;
};

/**
 * \fn int swloc_kernel_options_init(struct swloc_kernel_options * options)
 * \brief Initialize the structure swloc_kernel_options \p options to the default values.
 */
int swloc_kernel_options_init(struct swloc_kernel_options * options);

/**
 * \fn swloc_kernel_t swloc_kernel_start(swloc_context_t ctx, struct swloc_kernel_options * options)
 * \brief Launch the kernel on the context \p ctx with the \p options.
 */
swloc_kernel_t swloc_kernel_start(swloc_context_t ctx, struct swloc_kernel_options * options);

/**
 * \fn int swloc_kernel_wait(swloc_kernel_t knl)
 * \brief Wait until the kernel \p knl has finished.
 */
int swloc_kernel_wait(swloc_kernel_t knl);

#endif /* SWLOC_KERNEL_H */ 
