#ifndef SWLOC_H
#define SWLOC_H

#ifdef __cplusplus
extern "C" {
#endif

#include <swloc_context.h>
#include <swloc_core.h>
#include <swloc_hypervisor.h>
#include <swloc_policy.h>
#include <swloc_kernel.h>

/**
 * \fn int swloc_init()
 * \brief Initialize the SwLoc library.
 *
 * No call to any SwLoc functions has to be made before this function.
 */
int swloc_init();

/**
 * \fn int swloc_finalize()
 * \brief Finalize the SwLoc library.
 *
 * No call to any SwLoc functions has to be made after this function.
 */
int swloc_finalize();

#ifdef __cplusplus
}
#endif

#endif /* SWLOC_H */

