#ifndef SWLOC_CORE_H
#define SWLOC_CORE_H

/**
 * \fn swloc_context_t swloc_core_get_context()
 * \brief Return a pointer to actual context used.
 *
 * Use this function only inside a kernel function.
 */
swloc_context_t swloc_core_get_context();

#endif /* SWLOC_CORE_H */
